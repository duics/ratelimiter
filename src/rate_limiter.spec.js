import { RateLimiter } from "./rate_limiter";
const JITTER = 20;

function expectWaitTime(startTime, waitMs) {
  const date = Date.now();
  const errorMessage = `Ratelimiter should have fired approximately ${waitMs}ms from start, but instead it was fired after ${date -
    startTime}ms`;
  expect(date, errorMessage).toBeGreaterThanOrEqual(startTime + waitMs);
  expect(date, errorMessage).toBeLessThanOrEqual(startTime + waitMs + JITTER);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

describe("Rate Limiter", () => {
  it("resolves immediately when not limited", async () => {
    const limiter = new RateLimiter({ maxRequests: 1, timeWindowMs: 100 });
    const startTime = Date.now();
    await limiter.resolveRequest();
    expectWaitTime(startTime, 0);
  });

  it("waits when queue is full", async () => {
    const timeWindowMs = 100;
    const limiter = new RateLimiter({
      maxRequests: 1,
      timeWindowMs: timeWindowMs
    });
    const startTime = Date.now();
    await limiter.resolveRequest();
    await limiter.resolveRequest();
    expectWaitTime(startTime, timeWindowMs);
  });

  it("runs requests in order", async () => {
    const limiter = new RateLimiter({ maxRequests: 5, timeWindowMs: 1 });
    const callbacks = [];
    const indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    await Promise.all(
      indices.map(i => limiter.resolveRequest().then(callbacks.push(i)))
    );
    expect(callbacks).toStrictEqual(indices);
  });

  it("is efficient", async () => {
    const limiter = new RateLimiter({ maxRequests: 2, timeWindowMs: 50 });
    const startTime = Date.now();
    limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 0);
    });
    await sleep(5);
    limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 5);
    });
    await sleep(5);
    limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 50);
    });
    await sleep(5);
    limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 55);
    });
    await sleep(5);
    limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 100);
    });
    await sleep(5);
    await limiter.resolveRequest().then(() => {
      expectWaitTime(startTime, 105);
    });
  });
});


describe("Rate limiter internal functionality", () => {
  it("puts the request into windows if max requests not hit", () => {
    const limiter = new RateLimiter({ maxRequests: 2, timeWindowMs: 50 });
    limiter.resolveRequest();
    expect(limiter.windows.length).toBe(1);
  })

  it("puts the request into queue if max requests are hit", () => {
    const limiter = new RateLimiter({ maxRequests: 2, timeWindowMs: 50 });
    limiter.resolveRequest();
    limiter.resolveRequest();
    limiter.resolveRequest();
    expect(limiter.windows.length).toEqual(2);
    expect(limiter.queue.length).toEqual(1);
  })

  it("puts the request into queue if max requests are hit", () => {
    const limiter = new RateLimiter({ maxRequests: 2, timeWindowMs: 50 });
    limiter.resolveRequest();
    limiter.resolveRequest();
    limiter.resolveRequest();
    expect(limiter.windows.length).toEqual(2);
    expect(limiter.queue.length).toEqual(1);
  })

})
