/* Main entrypoint for the application */
import { fetchItem, addToItemList } from "./items";
import { RateLimiter } from "./rate_limiter";

console.log("In case you feel lost where to start, check src/app.js");

const rateLimiter = new RateLimiter({ maxRequests: 5, timeWindowMs: 11000 });
document.querySelector("#fetch-button").addEventListener("click", e => {
  rateLimiter.resolveRequest().then(() => fetchItem(item => addToItemList(item)));
});

document.querySelector("#fetch-many-button").addEventListener("click", e => {
  for (let i = 0; i < 10; i++) {
    rateLimiter.resolveRequest().then(() => fetchItem(item => addToItemList(item)));
  }
});
