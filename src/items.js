// Use random endpoint to reset ratelimit after reload.
const ENDPOINT = Math.floor(Math.random() * 10000);

export const fetchItem = (itemReceivedFn) =>
  fetch(`https://rate-limit-server.herokuapp.com/endpoint/${ENDPOINT}`)
    .then((response) => response.text())
    .then((item) => itemReceivedFn(item));

export const addToItemList = (item) => {
  const itemList = document.querySelector("#items");
  const element = document.createElement("li");
  element.appendChild(document.createTextNode(item));
  itemList.appendChild(element);
}
