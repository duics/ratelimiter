export class RateLimiter {


  constructor(options) {
    this.maxRequests = options.maxRequests;
    this.timeWindowMs = options.timeWindowMs;
    this.WINDOW_CLEANUP_PERIOD = 10;

    // Traces left by requests that were fired Should contain a timestamp to
    // determine whether it's ok to clean up.
    //
    // 1. Search from beginning, find the first timestamp that ISN'T out of
    // date, cut from beginning
    //
    // 2. Loop from the end and drop timestamps iteratively
    this.windows = [];

    // Requests yet to be fired
    this.queue = [];

    this.intitializeCleaner();
  }

  resolveRequest() {
    return new Promise(resolve => {
      if (this.windows.length < this.maxRequests) {
        this.executeRequest(resolve);
      } else {
        this.queue.push(resolve);
      }
    });
  }

  executeRequest(resolve) {
    this.windows.push(Date.now() + this.timeWindowMs);
    resolve();
  }

  intitializeCleaner() {
    setTimeout(() => this.cleanWindows(), this.WINDOW_CLEANUP_PERIOD);
  }

  cleanWindows() {
    const now = Date.now();
    this.windows = this.windows.filter(ts => ts < now);
    this.resolvePending();
    setTimeout(() => this.cleanWindows(), this.WINDOW_CLEANUP_PERIOD);
  }

  resolvePending() {
    const count = this.maxRequests - this.windows.length;
    const pendingRequests = this.queue.splice(0, count);
    pendingRequests.forEach(resolve => resolve());
  }
}
