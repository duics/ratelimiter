# Smartly.io Backend Coding Exercise

Your goal in this exercise is to implement rate limit client which resolve requests as soon as possible when we know that there is less than maxRequests in the past timeWindowMs.

## Dependencies

- Node >= 12.0.0
- npm >= 6.0.0

## Installation

Run `npm install` to install dependencies.

## Running the starter code

Run the webpack-development-server with `npm start` and point your browser to the address outputted by the previous command.

## Testing

`npm test`

## Requirements

Rate limit client is a web application that fetches items from internet with limitations by time and amount of requests.

You need to write your implementation of `resolveRequest` in `src/rate_limiter.js` which will resolve requests as soon as possible and won't get errors by exceeded request amount limits in time window (5 requests per 10 seconds limit means that you can send 5 requests at once and 6th request should be sent only after 10 seconds timeout). If you make first requests with 1 second delay between each other, scheduled requests should also be sent with 1 second interval as soon as they can be sent (Sliding Window algorithm).

You can use UI which is available when a development server is started. There you can click left button to send one request to a server and right button to send 10 request at once and see what it will show you (getting first results could takes couple of seconds because the server is running on Heroku instance and it need to be started on demand). When you will implement proper `resolveRequest` method you shouldn't get any errors in responses.

Also you can use tests to check that your implementation is passed all requirements.

We don't expect that you will able to wirte ideal implementation in one hour of interview. But we expect that we will have a good discussion and couple of possible implementation steps (don't be afraid to start with simple naive implementation, it could be a good initial point for future improvements).
